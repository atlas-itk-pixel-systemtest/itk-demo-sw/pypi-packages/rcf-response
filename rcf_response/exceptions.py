import inspect

class RcfException(Exception):
    def __init__(
        self,
        status,
        title,
        msg,
        status_id = 0,
        type = "about:blank",
        instance = None,
        ext = None
    ):
        self.status = status
        self.title = title
        self.msg = msg
        self.status_id = status_id
        self.type = type
        if instance is None:
            for s in inspect.stack():
                filename = s.filename[
                    s.filename.rfind("/") + 1 
                    : s.filename.rfind(".py")
                ]
                self.instance = f"{filename} - {s.function} (l. {s.lineno})"
                if not filename in ["errors", "exceptions"] and not s.function[0] == "_":
                    break
        else:
            self.instance = instance
        self.ext = ext
            
        super().__init__(f"{title} - {msg}")