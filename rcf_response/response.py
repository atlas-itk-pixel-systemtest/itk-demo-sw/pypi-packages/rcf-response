import inspect


class _output:
    def __init__(self):
        self.status = 0

    def is_ok(self):
        return self.status // 100 == 2

    def to_conn_response(self):
        print(
            "Method 'to_conn_response()' should be overwritten by "
            "specific subclass of _output."
        )


class Error(_output):
    def __init__(
        self,
        _status=400,
        _title="",
        _detail="",
        _status_id=0,
        _type="about:blank",
        _ext=None,
        _instance=None
    ):
        assert _status // 100 in [3, 4, 5]
        self.status = _status
        self.title = _title
        self.detail = _detail
        self.status_id = _status_id
        self.type = _type
        self.ext = _ext
        s = inspect.stack()[1]
        self.instance = (
            f"{__name__} - {s.function} (l. {s.lineno})"
            if _instance is None
            else _instance
        )

    @classmethod
    def from_exception(cls, e):
        try:
            return cls(
                _status = e.status,
                _title = e.title,
                _detail = e.msg,
                _status_id = e.status_id,
                _type = e.type,
                _ext = e.ext,
                _instance = e.instance
                
            )
        except AttributeError as e:
            return Error(
                500,
                "Exception Error",
                e.args[0]
            )

    def get_dict(self):
        return {
            "status": self.status,
            "title": self.title,
            "detail": self.detail,
            "instance": self.instance,
            "status_id": self.status_id
        }

    def __iter__(self):
        yield self.get_dict()
        yield self.status

    def to_conn_response(self):
        output = self.get_dict()
        if self.type is not None:
            output["type"] = self.type
        if self.ext is not None:
            output["ext"] = self.ext

        return output, self.status


class _basic_output(_output):

    def __init__(self):
        self.status = 0
        self.messages = []
        self.payload = {}

    def add_msg(self, message):
        self.messages.append(message)

    def to_conn_response(self):
        return {
            "status": self.status,
            "messages": self.messages,
            "payload": self.payload
        }, self.status


class Warning(_basic_output):
    def __init__(self, message, payload=None):
        self.status = 299
        self.messages = [message]
        self.payload = {} if payload is None else payload


class Info(_basic_output):
    def __init__(self, message="", payload=None):
        self.status = 200
        self.messages = [message] if message != "" else []
        self.payload = {} if payload is None else payload
